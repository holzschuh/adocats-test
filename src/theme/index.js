import { createMuiTheme } from '@material-ui/core';

import palette from './palette';

const theme = createMuiTheme({
  palette,
  shape: {
    borderRadius: 16
  },
  zIndex: {
    appBar: 1200,
    drawer: 1100
  },
  typography: {
    allVariants: {
     fontFamily: 'Montserrat, sans-serif'     
    }
  },
});

export default theme;

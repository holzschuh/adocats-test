import React from 'react';
import Topbar from './components/Topbar';

const Main: React.FC = (props) => {
  const { children } = props;

  return (
    <div>
      <Topbar />
      <div style={{ marginTop: '5rem' }}>
        {children}
      </div>
    </div>
  );
}

export default Main;

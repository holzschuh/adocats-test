import React from 'react';
import { AppBar, Typography, Toolbar, Button, Container } from '@material-ui/core';
import { makeStyles, Theme, createStyles } from '@material-ui/core';
import { NavLink as Link } from 'react-router-dom';
import AuthActions from '../../../../actions/AuthActions';
import AuthStore from '../../../../stores/AuthStore';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      // flexGrow: 1,
    },
    menuButton: {
      marginRight: theme.spacing(2),
    },
    title: {
      flexGrow: 1,
      fontFamily: 'Montserrat, sans-serif',
      fontSize: '2rem'
    },
    toolbar: {
      backgroundColor: theme.palette.primary.main
    }
  }),
);

const Topbar: React.FC = () => {
  const classes = useStyles();

  const [isLogged, setLogged] = React.useState(AuthStore.isLoggedIn())

  React.useEffect(() => {
    _authChange();
    AuthStore.addChangeListener(_authChange);
    return () => {
      AuthStore.removeChangeListener(_authChange);
    }
  }, []);

  const _authChange = () => {
    setLogged(AuthStore.isLoggedIn())
  }

  const handleLogout = () => {
    AuthActions.logout();
    setLogged(false);
  };

  return (
    <div className={classes.root}>
      <AppBar position="fixed" className={classes.toolbar}>
        <Container>
          <Toolbar>
            <Typography variant="h6" className={classes.title}>
              <Link to="/" style={{ textDecoration: 'none', color: 'inherit' }}>
                adocats
                            </Link>
            </Typography>
            {isLogged && <Button color="inherit" onClick={handleLogout}>Logout</Button>}
            {!isLogged && <Link to="login" style={{ textDecoration: 'none', color: 'inherit' }} ><Button color="inherit">Sign In</Button></Link>}
          </Toolbar>
        </Container>
      </AppBar>
    </div>
  );
};

export default Topbar;

import AppDispatcher from '../dispatcher/AppDispatcher';
import Constants from '../actions/Constants';
import EventEmitter from 'events';
import API from '../services/API';

const CHANGE_EVENT = 'CHANGE_EVENT';
let _result = {result: [], count: 0, pages: 0 };

class PetStore extends EventEmitter {
    constructor() {
        super();
        AppDispatcher.register(this.registerActions.bind(this));
    }

    registerActions(action) {
        switch (action.actionType) {
            case Constants.SEARCH_PETS:
                API.searchPets(action.options);
                this.emit(CHANGE_EVENT);
                break;
            case Constants.SHOW_RESULTS:
                this.setResult(action.result);
                this.emit(CHANGE_EVENT);
                break;
            default:
        }
        return true;
    }

    setResult(result) {
        _result = result;
    }

    getResult() {
        return _result;
    }

    addChangeListener(callback) {
        this.on(CHANGE_EVENT, callback);
    }

    removeChangeListener(callback) {
        this.removeListener(CHANGE_EVENT, callback);
    }

}

export default new PetStore();
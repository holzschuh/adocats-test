import AppDispatcher from '../dispatcher/AppDispatcher';
import Constants from '../actions/Constants';
import EventEmitter from 'events';

const CHANGE_EVENT = 'AUTH_EVENT';

class AuthStore extends EventEmitter {
  constructor() {
    super();
    AppDispatcher.register(this.registerActions.bind(this));
  }

  registerActions(action) {
    switch (action.actionType) {
      case Constants.LOGIN_USER:
        this.emit(CHANGE_EVENT);
        break;
      case Constants.LOGOUT:
        this.emit(CHANGE_EVENT);
        break;
      default:
    }
    return true;
  }

  isLoggedIn() {
    return localStorage.getItem('token') != null ? true : false;
  }

  addChangeListener(callback) {
    this.on(CHANGE_EVENT, callback);
  }

  removeChangeListener(callback) {
    this.removeListener(CHANGE_EVENT, callback);
  }

}

export default new AuthStore();
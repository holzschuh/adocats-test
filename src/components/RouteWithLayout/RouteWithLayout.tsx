import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import AuthStore from '../../stores/AuthStore';

type Props = {
  layout: React.FC<object>,
  component: any,
  path: string,
  auth: boolean
};

const RouteWithLayout: React.FC<Props> = (props: Props) => {
  const { layout: Layout, component: Component, auth, ...rest } = props;

  const [isLogged, setLogged] = React.useState(false);

  React.useEffect(() => {
    _authChange();
    AuthStore.addChangeListener(_authChange);
    return () => {
      AuthStore.removeChangeListener(_authChange);
    }
  }, []);

  const _authChange = () => {
    setLogged(AuthStore.isLoggedIn())
  }

  return (
    <>
    {(!auth || isLogged) ? (
      <Route
        {...rest}
        render={matchProps => (
          <Layout>
            <Component {...matchProps} />
          </Layout>
        )}
    /> ): 
      <Redirect to="login"/>})
    </>
  );
};

export default RouteWithLayout;

import React from 'react';
import { Router, Switch, Redirect } from 'react-router-dom';
import { createBrowserHistory } from 'history';

import RouteWithLayout from '../RouteWithLayout';
import Main from '../../layouts/Main';
import SignInView from '../../views/SignIn/SignIn';
import SearchView from '../../views/Search/Search';
import { ThemeProvider } from '@material-ui/core';

import theme from '../../theme/index';

const history = createBrowserHistory();

const App: React.FC = () => {  
  return (
    <ThemeProvider theme={theme}>
    <div>
      <Router history={history}>
        <Switch>
          <Redirect exact from="/" to="login"/>
          <RouteWithLayout
            auth={false}
            layout={Main}
            component={SignInView}
            path={'/login'}
          />
          <RouteWithLayout
            auth={true}
            layout={Main}
            component={SearchView}
            path={'/search'}
          />
        </Switch>
      </Router>
    </div>
    </ThemeProvider>

  );
}

export default App;

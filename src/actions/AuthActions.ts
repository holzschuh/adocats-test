import Dispatcher from '../dispatcher/AppDispatcher';
import C from './Constants';

class Actions {
  loginUser(token: string) {
    localStorage.setItem('token', token);

    Dispatcher.dispatch({
      actionType: C.LOGIN_USER,
      token
    });
  }

  logout() {
    localStorage.removeItem('token');

    Dispatcher.dispatch({
      actionType: C.LOGOUT
    });
  }

}

export default new Actions();
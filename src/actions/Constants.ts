const Constants = {
  LOGIN_USER: 'LOGIN_USER',
  LOGOUT: 'LOGOUT',

  SEARCH_PETS: 'SEARCH_PETS',
  SHOW_RESULTS: 'SHOW_RESULTS'
};

export default Constants;
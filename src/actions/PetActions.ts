import Dispatcher from '../dispatcher/AppDispatcher';
import C from './Constants';

class Actions {
  searchPets(options: object) {
    Dispatcher.dispatch({
      actionType: C.SEARCH_PETS,
      options
    });
  }

  showResults(result: object) {
    Dispatcher.dispatch({
      actionType: C.SHOW_RESULTS,
      result
    });
  }
}

export default new Actions();
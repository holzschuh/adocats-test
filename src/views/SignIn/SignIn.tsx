import React, { FormEvent } from 'react';
import { Typography, Container, Paper, TextField, Button, CssBaseline, Backdrop, CircularProgress, Snackbar } from '@material-ui/core';
import { Alert } from '@material-ui/lab';
import { makeStyles, Theme, createStyles } from '@material-ui/core';
import AuthService from '../../services/AuthService';
import { Redirect } from 'react-router';
import AuthStore from '../../stores/AuthStore';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      marginTop: theme.spacing(3)
    },
    paper: {
      padding: theme.spacing(2, 4),
      paddingTop: theme.spacing(5)
    },
    submit: {
      margin: theme.spacing(3, 0, 2),
      padding: theme.spacing(2),
    },
    subtitle: {
      color: '#999',
      marginTop: theme.spacing(1),
      marginBottom: theme.spacing(2),
    },
    backdrop: {
      zIndex: theme.zIndex.drawer + 1,
      color: '#fff',
    },
  }),
);

const SignIn: React.FC = () => {
  const classes = useStyles();

  const [email, setEmail] = React.useState('');
  const [password, setPassword] = React.useState('');
  const [open, setOpen] = React.useState(false);
  const [hasError, setHasError] = React.useState(false);
  const [isLogged, setIsLogged] = React.useState(false);

  React.useEffect(() => {
    if(AuthStore.isLoggedIn()) setIsLogged(true);
  }, [])

  const handleSubmit = (event: FormEvent) => {
    event.preventDefault();

    setOpen(true);
    AuthService.login(email, password).then(res => {
      setOpen(false);
      setIsLogged(true);
    }).catch(err => {
      console.error(err);
      setOpen(false);
      setHasError(true);
      setTimeout(() => setHasError(false), 3000)
    });
  }

  return (
    <>
      { isLogged && <Redirect to="search" /> }
      <Backdrop className={classes.backdrop} open={open}>
        <CircularProgress color="inherit" />
      </Backdrop>
      <Snackbar open={hasError}>
        <Alert elevation={6} variant="filled" severity="error">
          Oops! Incorrect credentials!
        </Alert>
      </Snackbar>
      <Container className={classes.root}>
        <CssBaseline />
        <Paper className={classes.paper}>
          <Typography variant="h4">
            Sign In
                </Typography>
          <Typography className={classes.subtitle}>
            Sign in to search your new friend...
          </Typography>
          <form onSubmit={handleSubmit}>
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="email"
              label="Email Address"
              name="email"
              autoComplete="email"
              onChange={(event) => setEmail(event.target.value)}
              autoFocus
            />
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="password"
              label="Password"
              name="password"
              type="password"
              onChange={(event) => setPassword(event.target.value)}
            />
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
            >
              Sign In
            </Button>
          </form>
        </Paper>
      </Container>
    </>
  );
};

export default SignIn;

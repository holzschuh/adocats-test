import React from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Typography from '@material-ui/core/Typography';
import { CircularProgress } from '@material-ui/core';
import TableToolbar from './TableToolbar';
import { Filter, Row } from '../../Types';
import { Error as IconEmpty } from '@material-ui/icons';

export type Order = 'asc' | 'desc';

export interface HeadCell {
  id: string;
  label: string;
};

const LoadingIndicator: React.FC = () => {
  return (
    <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', flexDirection: 'column', minHeight: '20rem' }}>
      <CircularProgress />
      <Typography component="p" style={{ marginTop: '1rem' }}>Loading</Typography>
    </div>
  );
};

const EmptyDataset: React.FC = () => {
  return (
    <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', flexDirection: 'column', minHeight: '20rem' }}>
      <IconEmpty fontSize="large" color="primary" />
      <Typography variant="h4" style={{ marginTop: '1rem' }}>Oops!</Typography>
      <Typography component="p" style={{ marginTop: '1rem' }}>No results, try again</Typography>
    </div>
  );
};

interface EnhancedTableProps {
  classes: ReturnType<typeof useStyles>,
  headCells: HeadCell[],
  onRequestSort: (event: React.MouseEvent<unknown>, property: string) => void,
  order: Order,
  orderBy: string,
};

function EnhancedTableHead({ classes, headCells, order, orderBy, onRequestSort }: EnhancedTableProps) {
  const createSortHandler = (property: string) => (event: React.MouseEvent<unknown>) => {
    onRequestSort(event, property);
  };

  return (
    <TableHead>
      <TableRow>
        {headCells.map(headCell => (
          <TableCell
            key={headCell.id}
            sortDirection={orderBy === headCell.id ? order : false}
          >
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={order}
              onClick={createSortHandler(headCell.id)}
            >
              <b>{headCell.label}</b>
              {orderBy === headCell.id ? (
                <span className={classes.visuallyHidden}>
                  {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                </span>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    table: {
      minWidth: 750,
    },
    visuallyHidden: {
      border: 0,
      clip: 'rect(0 0 0 0)',
      height: 1,
      margin: -1,
      overflow: 'hidden',
      padding: 0,
      position: 'absolute',
      top: 20,
      width: 1,
    },
  }),
);

interface TableProps {
  isLoading: boolean,

  headCells: HeadCell[],
  rows: Row[],
  itemsCount: number,
  page: number,
  perPage: number,

  order: Order,
  orderBy: string,

  onPageChange: (event: unknown, newPage: number) => void,
  onRowsPerPageChange: (event: React.ChangeEvent<HTMLInputElement>) => void,
  onRequestSort: (event: React.MouseEvent<unknown>, property: string) => void,
  onFilterChange: (filters: Filter) => void
};

export default function TableComponent(props: TableProps) {
  const classes = useStyles();

  const { headCells, rows, itemsCount, page, perPage, order, orderBy, isLoading } = props;
  const { onPageChange, onRowsPerPageChange, onRequestSort, onFilterChange } = props;

  const emptyRows = perPage - Math.min(perPage, itemsCount - page * perPage);

  return (
    <div>
      <TableToolbar tableTitle="Pet search" onFilterChange={onFilterChange} />
      <TableContainer>
        <Table
          className={classes.table}
          size="medium"
        >
          <EnhancedTableHead
            classes={classes}
            headCells={headCells}
            order={order}
            orderBy={orderBy}
            onRequestSort={onRequestSort}
          />
          {!isLoading && itemsCount !== 0 &&
            <TableBody>
              {rows.map((row, index) => {
                return (
                  <TableRow
                    hover
                    tabIndex={-1}
                    key={row.name}
                  >
                    {headCells.map((e, i) => 
                        <TableCell key={i}>{row[e.id]}</TableCell>
                    )}
                  </TableRow>
                );
              })}
              {emptyRows > 0 && (
                <TableRow style={{ height: 53 * emptyRows }}>
                  <TableCell colSpan={6} />
                </TableRow>
              )}
            </TableBody>
          }
        </Table>
      </TableContainer>

      {itemsCount === 0 && !isLoading && <EmptyDataset />}
      {isLoading && <LoadingIndicator />}

      <TablePagination
        rowsPerPageOptions={[5, 10, 25]}
        component="div"
        count={itemsCount || rows.length}
        rowsPerPage={perPage}
        page={page}
        onChangePage={onPageChange}
        onChangeRowsPerPage={onRowsPerPageChange}
      />
    </div>
  );
}
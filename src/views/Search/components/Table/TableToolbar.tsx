import React from 'react';
import { makeStyles } from '@material-ui/styles';
import { Theme, createStyles, Toolbar, Typography, Tooltip, IconButton, Dialog, DialogTitle, DialogContent, DialogActions, Button, useMediaQuery, FormControl, InputLabel, Select, Input, MenuItem } from '@material-ui/core';
import FilterListIcon from '@material-ui/icons/FilterList';
import theme from '../../../../theme';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      paddingLeft: theme.spacing(2),
      paddingRight: theme.spacing(1),
    },
    title: {
      flex: '1 1 100%',
    },
    container: {
      display: 'flex',
      flexWrap: 'wrap',
    },
    formControl: {
      margin: theme.spacing(1),
      minWidth: 120,
    },
  }),
);

interface TableToolbarProps {
  tableTitle: string
  onFilterChange: (filters: {
    sex_key: string,
    size_key: string,
    age_key: string,
  }) => void
};

const TableToolbar: React.FC<TableToolbarProps> = (props: TableToolbarProps) => {
  const classes = useStyles();
  const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));
  const [open, setOpen] = React.useState(false)

  const [filters, setFilters] = React.useState({
    sex_key: '',
    size_key: '',
    age_key: ''
  });

  const handleOpenFilter = () => setOpen(true);
  const handleCloseFilter = () => setOpen(false);

  const handleChange = (event: React.ChangeEvent<{ name?: string; value: unknown }>, child: React.ReactNode) => {
    const key = event.target.name as string
    setFilters({
      ...filters,
      [key]: event.target.value
    })
  };

  const handleSubmit = () => {
    props.onFilterChange(filters);
    setOpen(false);
  }

  return (
    <>
      <Toolbar>
        <Typography className={classes.title} variant="h6" id="tableTitle">
          {props.tableTitle || 'Pets'}
        </Typography>
        <Tooltip title="Filter list">
          <IconButton aria-label="filter list" onClick={handleOpenFilter}>
            <FilterListIcon />
          </IconButton>
        </Tooltip>
      </Toolbar>

      <Dialog
        fullScreen={fullScreen}
        open={open}
        aria-labelledby="responsive-dialog-title"
      >
        <DialogTitle id="responsive-dialog-title">{"Filter the results"}</DialogTitle>
        <DialogContent>
          <form className={classes.container}>
            <FormControl className={classes.formControl}>
              <InputLabel id="select-label-sex">Sex</InputLabel>
              <Select
                labelId="select-label-sex"
                id="sex"
                name="sex_key"
                value={filters.sex_key}
                onChange={handleChange}
                input={<Input />}
              >
                <MenuItem value="">
                  <em>Any</em>
                </MenuItem>
                <MenuItem value="MALE">Male</MenuItem>
                <MenuItem value="FEMALE">Female</MenuItem>
              </Select>
            </FormControl>
            <FormControl className={classes.formControl}>
              <InputLabel id="select-label-size">Size</InputLabel>
              <Select
                labelId="select-label-size"
                id="size"
                name="size_key"
                value={filters.size_key}
                onChange={handleChange}
                input={<Input />}
              >
                <MenuItem value="">
                  <em>Any</em>
                </MenuItem>
                <MenuItem value="XS">Smaller</MenuItem>
                <MenuItem value="S">Small</MenuItem>
                <MenuItem value="M">Medium</MenuItem>
                <MenuItem value="L">Large</MenuItem>
                <MenuItem value="XL">Larger</MenuItem>
              </Select>
            </FormControl>
            <FormControl className={classes.formControl}>
              <InputLabel id="select-label-age">Age</InputLabel>
              <Select
                labelId="select-label-age"
                id="age"
                name="age_key"
                // value={10}
                value={filters.age_key}
                onChange={handleChange}
                input={<Input />}
              >
                <MenuItem value="">
                  <em>Any</em>
                </MenuItem>
                <MenuItem value="BABY">Baby</MenuItem>
                <MenuItem value="YOUNG">Young</MenuItem>
                <MenuItem value="ADULT">Adult</MenuItem>
                <MenuItem value="SENIOR">Senior</MenuItem>
              </Select>
            </FormControl>
          </form>
        </DialogContent>
        <DialogActions>
          <Button autoFocus onClick={handleCloseFilter} color="primary">
            Cancel
          </Button>
          <Button onClick={handleSubmit} color="primary" variant="outlined" autoFocus>
            Filter
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
};

export default TableToolbar;
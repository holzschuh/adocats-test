export interface Filter {
    sex_key: string,
    size_key: string,
    age_key: string
};

export interface Data {
    name: string,
    sex_key: string,
    size_key: string,
    age_key: string
};

export interface Row {
    name: string,
    sex_key: string,
    size_key: string,
    age_key: string,
    [x: string]: string
};

export interface Result {
    result: Row[],
    pages: number,
    count: number
};
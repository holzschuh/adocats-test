import React from 'react';
import TableComponent, { HeadCell, Order } from './components/Table/Table';

import { Container, Paper, makeStyles, Theme, createStyles } from '@material-ui/core';
import PetActions from '../../actions/PetActions';
import PetStore from '../../stores/PetStore';
import { Filter, Row, Result } from './Types';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%',
    },
    paper: {
      width: '100%',
      marginBottom: theme.spacing(2),
    },
  })
);

const SearchPage: React.FC = () => {

  const [isLoading, setIsLoading] = React.useState(true);
  const [page, setPage] = React.useState(0);
  const [perPage, setPerPage] = React.useState(5);
  const [order, setOrder] = React.useState<Order>('asc');
  const [orderBy, setOrderBy] = React.useState<string>('name');
  const [filters, setFilters] = React.useState({
    sex_key: '',
    size_key: '',
    age_key: ''
  });
  const [result, setResult] = React.useState<Result>({
    result: [],
    count: 0,
    pages: 0
  });

  React.useEffect(() => {
    PetStore.addChangeListener(_onChange);
    return () => {
      PetStore.removeChangeListener(_onChange);
    }
  }, []);

  React.useEffect(() => {
    fetchData();
    setIsLoading(true);
  }, [page, perPage, order, orderBy, filters]);

  const classes = useStyles();

  const headCells: HeadCell[] = [
    { id: 'name', label: 'Name' },
    { id: 'sex_key', label: 'Sex' },
    { id: 'size_key', label: 'Size' },
    { id: 'age_key', label: 'Age' },
    { id: 'price', label: 'Price' },
    // { id: 'payment_model_key', label: 'Pay Model' },
    // { id: 'status_key', label: 'Status' },
  ];

  const _onChange = () => {
    let result = PetStore.getResult();
    setResult(result);
    setIsLoading(false);
  };

  const fetchData = () => {
    // sort
    let sort = [(order === 'desc' ? '-' : '').concat(orderBy)];

    let req = {
      search: {
        sex_key: filters.sex_key,
        size_key: filters.size_key,
        age_key: filters.age_key,
      },
      options: {
        page: page + 1,
        limit: perPage,
        sort: sort
      }
    };

    PetActions.searchPets(req);
  };

  const handlePageChange = (event: unknown, newPage: number) => setPage(newPage);

  const handleRowsPerPage = (event: React.ChangeEvent<HTMLInputElement>) => {
    setPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleRequestSort = (event: React.MouseEvent<unknown>, property: string) => {
    const isDesc = orderBy === property && order === 'desc';
    setOrder(isDesc ? 'asc' : 'desc');
    setOrderBy(property);
  };

  const handleFilter = (filters: Filter) => {
    setFilters(filters);
  };

  return (
    <Container>
      <div className={classes.root}>
        <Paper className={classes.paper}>
          <TableComponent
            isLoading={isLoading}
            headCells={headCells}
            rows={result.result as Row[]}
            itemsCount={result.count}
            page={page}
            perPage={perPage}
            order={order}
            orderBy={orderBy}
            onPageChange={handlePageChange}
            onRowsPerPageChange={handleRowsPerPage}
            onRequestSort={handleRequestSort}
            onFilterChange={handleFilter}
          />
        </Paper>
      </div>
    </Container>
  );
}

export default SearchPage;
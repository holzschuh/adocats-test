import axios from './axios.instance';
import AuthActions from '../actions/AuthActions';

const API_KEY = '391dccd7-3671-4a9f-a336-321290118231';

const getSessionToken = () => new Promise((resolve, reject) => {
  let data = {
    "system_api_key": API_KEY
  };
  axios.post('/auth/session-request', data).then(response => {
    resolve(response.data.data.access_key);
  }).catch(error => {
    reject(error);
  })
})

export default {
  login(email, password) {
    return new Promise(async (resolve, reject) => {

      let token = await getSessionToken();

      let data = {
        organization_user: { email, password }
      };
      axios.post('/auth/session-register', data, { headers: { authorization: 'Bearer ' + token } } )
        .then(response => {
          if (response.data.status !== 200) 
            reject(response.data);
          
          AuthActions.loginUser(response.data.data.access_key);
          resolve(response.data);
        })
        .catch(error => reject(error));
    })
  }
}